import type { Receipt } from '@/types/Receipt'
import http from './http'
import type { ReceiptItem } from '@/types/ReceiptItem'
type ReceiptDTO ={
    orderItems: {
      productId: number;
      qty: number;
    }[];
    userId: number;
  }
function addOrder(receipt:Receipt,receiptItems:ReceiptItem[]) {
    const receiptDTO:ReceiptDTO = {
        orderItems: [],
        userId: 0
    }
    receiptDTO.userId = receipt.userId
   receiptDTO.orderItems = receiptItems.map((item)=>{
        return {
            productId:item.productId,
            qty: item.unit
        }
    })
    
    
  return http.post('/orders', receiptDTO)
}
export default {addOrder}